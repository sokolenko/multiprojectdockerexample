#!/usr/bin/env bash
echo Start script...
cd spring-services || exit
sh  build-spring-services.sh
cd ..
cd app-services || exit
sh build-app-services.sh
echo End script...