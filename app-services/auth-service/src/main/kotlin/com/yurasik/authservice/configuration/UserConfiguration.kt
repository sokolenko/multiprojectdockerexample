package com.yurasik.authservice.configuration

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter
import org.springframework.security.crypto.password.PasswordEncoder

@Configuration
class UserConfiguration : GlobalAuthenticationConfigurerAdapter() {

    @Autowired
    private lateinit var passwordEncoder: PasswordEncoder

    @Throws(Exception::class)
    override fun init(auth: AuthenticationManagerBuilder) {
        auth.inMemoryAuthentication().withUser("admin")
            .password(passwordEncoder.encode("admin"))
            .roles("USER", "ADMIN", "MANAGER").authorities("CAN_READ", "CAN_WRITE", "CAN_DELETE").and()
            .withUser("user").password(passwordEncoder.encode("user"))
            .roles("USER").authorities("CAN_READ", "CAN_WRITE")
    }
}
