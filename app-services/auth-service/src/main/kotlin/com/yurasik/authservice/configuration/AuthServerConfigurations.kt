package com.yurasik.authservice.configuration

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer

@Configuration
class AuthServerConfigurations : WebSecurityConfigurerAdapter(), AuthorizationServerConfigurer {

    @Autowired
    private lateinit var authenticationManager: AuthenticationManager

    @Autowired
    private lateinit var passwordEncoder: PasswordEncoder

    @Bean
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Throws(Exception::class)
    override fun configure(security: AuthorizationServerSecurityConfigurer) {
        security.checkTokenAccess("permitAll()")
    }

    @Throws(Exception::class)
    override fun configure(client: ClientDetailsServiceConfigurer) {
        client.inMemory().withClient("authserver")
            .secret(passwordEncoder.encode("passwordforauthserver"))
            .scopes("READ", "WRITE")
            .accessTokenValiditySeconds(300)
            .refreshTokenValiditySeconds(3600)
            .authorizedGrantTypes("password", "authorization_code", "refresh_token")
    }

    @Throws(Exception::class)
    override fun configure(endpoint: AuthorizationServerEndpointsConfigurer) {
        endpoint.authenticationManager(authenticationManager)
    }
}