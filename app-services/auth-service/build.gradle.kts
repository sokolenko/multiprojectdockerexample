import org.jetbrains.kotlin.cli.jvm.main

plugins {
	kotlin("jvm")
}

dependencies {
	(kotlin("stdlib-jdk8"))
	compile("com.fasterxml.jackson.module:jackson-module-kotlin")

	compile("org.springframework.boot:spring-boot-starter-web")
	compile("org.springframework.cloud:spring-cloud-starter-oauth2:${rootProject.extra.get("springBootCloudVersion") as String}")
	compile("org.springframework.cloud:spring-cloud-starter-security:${rootProject.extra.get("springBootCloudVersion") as String}")
	compile("org.springframework.cloud:spring-cloud-starter-config:${rootProject.extra.get("springBootCloudVersion") as String}")
	compile("org.springframework.cloud:spring-cloud-starter-netflix-eureka-client:2.1.3.RELEASE")

	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.junit.jupiter:junit-jupiter-api")
	testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

docker {
	springBootApplication {
		baseImage.set("openjdk:8-alpine")
		ports.set(listOf(8070))
		tag.set("auth-service")
		maintainer.set("yurasik")
	}
}
