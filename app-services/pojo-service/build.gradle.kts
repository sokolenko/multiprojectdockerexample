
plugins {
    kotlin("jvm")
}

dependencies {
    (kotlin("stdlib-jdk8"))
    compile("org.springframework.boot:spring-boot-starter-actuator")
    compile("org.springframework.boot:spring-boot-starter-web")
    compile("org.springframework.cloud:spring-cloud-starter-config:2.1.4.RELEASE")
    compile("org.springframework.cloud:spring-cloud-starter-netflix-eureka-client:2.1.3.RELEASE")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    implementation(kotlin("stdlib-jdk8"))
}

docker {
    springBootApplication {
        baseImage.set("openjdk:8-alpine")
        ports.set(listOf(8500))
        tag.set("pojo-service")
        maintainer.set("yurasik")
    }
}