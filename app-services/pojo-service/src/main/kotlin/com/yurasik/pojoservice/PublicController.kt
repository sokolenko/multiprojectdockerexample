package com.yurasik.pojoservice

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController("/pojo")
class PublicController {

    @Autowired
    var environment: Environment? = null

    @Value("\${sample.pojoProperty}")
    var pojoProperty: String? = null

    @RequestMapping(method = [RequestMethod.GET], produces = [MediaType.APPLICATION_JSON_VALUE])
    @Throws(SecurityException::class)
    fun index(): String {
        val appsProperty = environment?.getProperty("sample.appsProperty");
        return "Hello from public API controller, app props: $appsProperty, \n " +
                "pojo property: ${pojoProperty}"
    }

}
