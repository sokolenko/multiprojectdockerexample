import com.bmuschko.gradle.docker.tasks.image.DockerBuildImage
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension

plugins {
    base
    java
    id("io.spring.dependency-management") version "1.0.8.RELEASE"
    kotlin("jvm") version "1.3.50" apply false
    id("org.jetbrains.kotlin.plugin.spring") version "1.3.50" apply false
    id("org.springframework.boot") version "2.1.9.RELEASE" apply false
    id("com.bmuschko.docker-spring-boot-application") version "5.2.0" apply false
}

val dockerBuildDir = "build/docker/"

allprojects {
    group = "com.yurasik"
    version = "1.0-SNAPSHOT"

    repositories {
        jcenter()
        mavenCentral()
    }
}

subprojects {

    if (name.contains("service", true)) {
        println("Enabling Kotlin Spring plugin in project ${project.name}...")
        apply(plugin = "org.springframework.boot")

        apply(plugin = "org.jetbrains.kotlin.plugin.spring")

        println("Enabling Spring Boot Dependency Management in project ${project.name}...")
        apply(plugin = "io.spring.dependency-management")
        the<DependencyManagementExtension>().apply {
            imports {
                mavenBom(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES)
            }
        }

        apply(plugin = "com.bmuschko.docker-spring-boot-application")
    }

    tasks.withType<KotlinCompile>().configureEach {
        println("Configuring $name in project ${project.name}...")
        kotlinOptions {
            jvmTarget = "1.8"
            freeCompilerArgs = listOf("-Xjsr305=strict")
        }
    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }

    tasks.withType<DockerBuildImage> {
        println("DockerBuildImage started: $this")
    }

}

dependencies {
    // Make the root project archives configuration depend on every subproject
    subprojects.forEach {
        archives(it)
    }
}