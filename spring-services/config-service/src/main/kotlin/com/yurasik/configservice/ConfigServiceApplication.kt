package com.yurasik.configservice

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.config.server.EnableConfigServer
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@SpringBootApplication
@EnableEurekaClient
@EnableConfigServer
class ConfigServiceApplication : CommandLineRunner {
    override fun run(vararg args: String?) {
        println("${javaClass.simpleName} service run...")
    }

}

fun main(args: Array<String>) {
    runApplication<ConfigServiceApplication>(*args)
}