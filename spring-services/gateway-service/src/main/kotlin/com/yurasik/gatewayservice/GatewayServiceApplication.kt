package com.yurasik.gatewayservice

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.cloud.netflix.zuul.EnableZuulProxy

@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy
class GatewayServiceApplication : CommandLineRunner {

    override fun run(vararg args: String?) {
        println("${javaClass.simpleName} service run...")
    }

}

fun main(args: Array<String>) {
    runApplication<GatewayServiceApplication>(*args)
}